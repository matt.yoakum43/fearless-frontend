function createCard(title, description, pictureUrl, startDateFormatted, endDateFormatted, locationSub) {
    return `
      <div class="card  row">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationSub}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDateFormatted} UNTIL ${endDateFormatted} </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
        // Figure out what to do when the response is bad
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const locationSub = details.conference.location.name;
                const startDate = details.conference.starts;
                const startDateFormatted = new Date(startDate);
                const endDate = details.conference.ends;
                const endDateFormatted = new Date(endDate);
                const html = createCard(name, description, pictureUrl, startDateFormatted, endDateFormatted, locationSub);
                const column = document.querySelector('.col');
                column.innerHTML += html;
            }
        }

    }
} catch (e) {
    // Figure out what to do if an error is raised
}

});
